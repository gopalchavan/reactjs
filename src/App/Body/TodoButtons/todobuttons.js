import React from 'react';

export default function TodoButtons(props){
    function handleClick(val){
        props.onShowStatusChange(val);
    }
    return(
        <div>
            <button onClick={()=>handleClick('all')}>All</button>
            <button onClick={()=>handleClick('pending')}>Active</button>
            <button onClick={()=>handleClick('completed')}>Completed</button>
        </div>
    )
}
