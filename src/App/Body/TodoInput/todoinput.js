import React from 'react';

export default class TodoInput extends React.Component{
    constructor(props){
        super();
        this.state = {
            todoName:''
        }
        this.AddTodo = this.AddTodo.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.todoInputRef = React.createRef();
    }
    componentDidMount(){
        this.todoInputRef.current.focus();
    }
    AddTodo(){
        this.props.onTodoAdd(this.state.todoName);
        this.setState({
            todoName: ''
        })
    }
    handleChange(e){
        this.setState({
            todoName: e.target.value
        })
    }
    render(){
        return(
            <div>
                <input type="text" ref={this.todoInputRef} value={this.state.todoName} onChange={this.handleChange}/>
                <button onClick={this.AddTodo}>Add Todo</button>
            </div>
        )
    }
}
