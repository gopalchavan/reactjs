import PropTypes from 'prop-types';
import React from 'react';
import './todolist.css'

export default function TodoList(props){
    function handleStatusChange(id){
        props.onStatusChange(id);
    }
    return(
        <ul>
            {props.todos.map((todo,ind) =>
                props.activeTab === 'all' || props.activeTab === todo.status ?
                <li key={todo.id} className={todo.status=="completed"?"strickClass":""} onClick={() => handleStatusChange(todo.id)}>
                    <span>{ind} </span>
                    <span> {todo.name}</span>
                </li> : false
            )}
        </ul>
    )
}

TodoList.prototype = {
    todos: PropTypes.array
}