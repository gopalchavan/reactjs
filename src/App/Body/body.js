import React from 'react';
import TodoInput from './TodoInput/todoinput';
import TodoButtons from './TodoButtons/todobuttons';
import TodoList from './TodoList/todolist';

export default class TodoBody extends React.Component{
    constructor(){
        super();
        this.state = {
            todos: [],
            activeTab: 'all',
            uniqueId: 0
        }
        this.addTodo = this.addTodo.bind(this);
        this.changeState = this.changeState.bind(this);
    }

    addTodo(item){
        this.setState((prevState,props) => {
            let tempTodos = prevState.todos;
            tempTodos.push({id:prevState.uniqueId,name:item,status:'pending'});
            return {todos: tempTodos,uniqueId: prevState.uniqueId+1}
        });
    }

    showSelectedItems(status){
        this.setState({
            activeTab: status
        })
    }
    changeState(id){
        debugger;        
        this.setState((prevState, props) => {
            let tempTodos = prevState.todos;
            let elem = tempTodos.find(elem => elem.id === id);
            if(elem.status === "pending"){
                elem.status = "completed";
            }else{
                elem.status = "pending";
            }
            return {todos: tempTodos}
        });
    }
    componentDidUpdate(prevProps, prevState){
        console.log(prevState);
        console.log(this.state);
    }
    render(){
        return(
            <React.Fragment>
                <TodoInput onTodoAdd={this.addTodo} />
                <TodoList todos = {this.state.todos} activeTab = {this.state.activeTab} onStatusChange = {this.changeState} />
                <TodoButtons onShowStatusChange={(status)=>this.showSelectedItems(status)} />
            </React.Fragment>
        )
    }
}
