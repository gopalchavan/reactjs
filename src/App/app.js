import React from 'react';
import Header from './Header/header';
import TodoBody from './Body/body';

export default function App(){
    return(
        <div>
            <Header />
            <TodoBody />
        </div>
    )
}
